#!/usr/bin/env bash

# Run app
. venv/bin/activate
#flask --app flaskr init-db
flask --app flaskr --debug run
