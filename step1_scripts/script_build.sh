#!/usr/bin/env bash

# Build package
. venv/bin/activate
pip install wheel
python setup.py bdist_wheel
