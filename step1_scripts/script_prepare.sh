#!/usr/bin/env bash

# Set up env
python3 -m venv venv
. venv/bin/activate
pip install -e .

# Run linter
pip install pylint
pylint flaskr

# Run tests
pip install '.[test]'
pytest

# Run coverage
pip install coverage
coverage run -m pytest
coverage report
