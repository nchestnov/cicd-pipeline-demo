# Создание CI/CD пайплайна для автоматизации разработки и развертывания приложения

Материал с вебинара команды ФПМИ МФТИ.

## О проекте

В качестве приложения, для которого будем создавать конвейер автоматизации, 
используем [Flaskr](https://github.com/pallets/flask/tree/main/examples/tutorial) - приложение на Python, написанное на фреймворке Flask.

В рамках вебинара рассмотрена эволюция CI/CD-автоматизации для данного проекта.

- [Шаг 0. Только код](https://gitlab.com/nchestnov/cicd-pipeline-demo/-/tree/main/step0_just_code) - для выполнения тестов и сборки будем использовать лишь команды в терминале, которые указаны в документации проекта
- [Шаг 1. Скрипты](https://gitlab.com/nchestnov/cicd-pipeline-demo/-/tree/main/step1_scripts) - сформируем и используем из известных команд скрипты
- [Шаг 2. Docker](https://gitlab.com/nchestnov/cicd-pipeline-demo/-/tree/main/step2_docker) - используем контейнеры Docker для изоляции среды тестирования и сборки
- [Шаг 3. GitLab CI/CD](https://gitlab.com/nchestnov/cicd-pipeline-demo/-/tree/main/step3_gitlab_cicd) - перейдем на современный инструмент автоматизации, [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) 